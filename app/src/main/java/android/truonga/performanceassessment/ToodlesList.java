package android.truonga.performanceassessment;

public class ToodlesList {

    public String Toodleslist;
    public String dateAdded;
    public String dateDue;
    public String category;

    public ToodlesList(String ToodlesList, String dateAdded,String dateDue,String category) {
        this.Toodleslist = ToodlesList;
        this.dateAdded = dateAdded;
        this.dateDue = dateDue;
        this.category = category;
    }
}
