package android.truonga.performanceassessment;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ToodlesListAdapter extends RecyclerView.Adapter<ToodlesListHolder> {

    private ArrayList<ToodlesList> toodlesList;
    private ActivityCallback activityCallback;


    public ToodlesListAdapter(ArrayList<ToodlesList> toodlesList, ActivityCallback activityCallback) {
        this.toodlesList = toodlesList;
        this.activityCallback = activityCallback;
    }

    @Override
    public int getItemCount() {
        return toodlesList.size();
    }

    @Override
    public ToodlesListHolder onCreateViewHolder(ViewGroup parent, int i) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            return new ToodlesListHolder(view);
    }

    @Override
    public void onBindViewHolder(ToodlesListHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCallback.onPostSelected(position);

            }
        });

        holder.titleText.setText(toodlesList.get(position).Toodleslist);
    }
}
