package android.truonga.performanceassessment;

import android.app.FragmentTransaction;
import android.support.annotation.LayoutRes;
import android.app.Fragment;
import java.util.ArrayList;

public class MaineActivity extends SingleFragmentActivity implements ActivityCallback {

    public ArrayList<ToodlesList> toodlesList = new ArrayList<ToodlesList>();
    public int currentItem;

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }


    @Override
    protected Fragment createFragment() {
        return new MaineListFragment();

    }

    @Override
    public void onPostSelected(int pos) {
        if (findViewById(R.id.detail_fragment_container) == null) {
            currentItem = pos;
            Fragment newFragment = new ItemViewFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        else {
            Fragment detailFragment = new ItemViewFragment();
            getSupportFragmentManager().beginTransaction();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.detail_fragment_container, detailFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
