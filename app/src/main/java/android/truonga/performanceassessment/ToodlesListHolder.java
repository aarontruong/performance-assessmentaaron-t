package android.truonga.performanceassessment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ToodlesListHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

        public ToodlesListHolder(View itemView) {
            super(itemView);
            titleText = (TextView)itemView;
        }
    }
