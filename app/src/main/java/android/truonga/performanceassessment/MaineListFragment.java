package android.truonga.performanceassessment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class MaineListFragment extends Fragment {

    private MaineActivity maineActivity;
    private ActivityCallback activityCallback;
    private RecyclerView recyclerView;

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.maineActivity = (MaineActivity) activity;
        activityCallback = (ActivityCallback) activity;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maine_list, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        ToodlesList toodlesList = new ToodlesList("Things", "About a Week AGOOOOO", "Today", "PerformanceAssesment");
        ToodlesList toodlesList2 = new ToodlesList("Thingses", "Added: 11/4..", "Added: 11/5..", "Added: 11/6..");
        ToodlesList toodlesList3 = new ToodlesList("Thingseses", "11/4..", "11/5..", "11/6");
        ToodlesList toodlesList4 = new ToodlesList("Thingsesesest", "CHORES", "HOMEWORK", "PENCILS");

        maineActivity.toodlesList.add(toodlesList);
        maineActivity.toodlesList.add(toodlesList2);
        maineActivity.toodlesList.add(toodlesList3);
        maineActivity.toodlesList.add(toodlesList4);


        ToodlesListAdapter adapter = new ToodlesListAdapter(maineActivity.toodlesList, activityCallback);
        recyclerView.setAdapter(adapter);

        return view;

    }
}

