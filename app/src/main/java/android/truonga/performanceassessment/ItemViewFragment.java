package android.truonga.performanceassessment;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


public class ItemViewFragment extends Fragment {

    private ViewPager viewPager;
    public int currentItem;
    private MaineActivity maineActivity;
    private ActivityCallback activityCallback;

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.maineActivity = (MaineActivity) activity;
        activityCallback = (ActivityCallback) activity;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_item_list, container, false);

        TextView tv1 = (TextView)layoutView.findViewById(R.id.TextView1);
        TextView tv2 = (TextView)layoutView.findViewById(R.id.TextView2);
        TextView tv3 = (TextView)layoutView.findViewById(R.id.TextView3);
        TextView tv4 = (TextView)layoutView.findViewById(R.id.TextView4);

        tv1.setText("Name: " + maineActivity.toodlesList.get(maineActivity.currentItem).Toodleslist);
        tv2.setText("DateAdded: " + maineActivity.toodlesList.get(maineActivity.currentItem).dateAdded);
        tv3.setText("DateDue: " + maineActivity.toodlesList.get(maineActivity.currentItem).dateDue);
        tv4.setText("Category: " + maineActivity.toodlesList.get(maineActivity.currentItem).category);

        return layoutView;

    }
}

